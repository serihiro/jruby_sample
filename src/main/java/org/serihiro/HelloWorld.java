package org.serihiro;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

class HelloWorld {
    public static void main(String[] args){
        System.out.println("Hello, World");

        ScriptEngineManager m = new ScriptEngineManager();
        ScriptEngine rubyEngine = m.getEngineByName("jruby");

        try {
            rubyEngine.eval("puts (1..10).to_a.inject { |sum, i| sum += i * i }");
        }
        catch (ScriptException e) {
            e.printStackTrace();
        }
    }
}
